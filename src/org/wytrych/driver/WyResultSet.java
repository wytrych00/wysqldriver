package org.wytrych.driver;

import org.wytrych.QueryResult.QueryResult;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.*;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Map;

public class WyResultSet implements ResultSet {
    private final QueryResult result;
    private Integer position = -1;

    WyResultSet(QueryResult result) {
        this.result = result;
    }

    public Integer getSize() {
        return result.size();
    }

    @Override
    public boolean next() throws SQLException {
        position++;
        boolean returnValue = result.size() > position;
        Logger.log("resultSize :" + result.size());
        Logger.log("next :" + returnValue);
        return returnValue;
    }

    @Override
    public void close() throws SQLException {
        Logger.log("close");

    }

    @Override
    public boolean wasNull() throws SQLException {
        Logger.log("wasNull");
        return false;
    }

    @Override
    public String getString(int columnIndex) throws SQLException {
        Logger.log("getString ---- " + columnIndex);
        return (String) result.get(position).get(columnIndex);
    }

    @Override
    public boolean getBoolean(int columnIndex) throws SQLException {
        Logger.log("getBoolean");
        int value = (int) result.get(position).get(columnIndex);
        return (value == 1);
    }

    @Override
    public byte getByte(int columnIndex) throws SQLException {
        Logger.log("getByte");
        return 0;
    }

    @Override
    public short getShort(int columnIndex) throws SQLException {
        Logger.log("getShort");
        return 0;
    }

    @Override
    public int getInt(int columnIndex) throws SQLException {
        Logger.log("getInt --- " + columnIndex);
        return 0;
    }

    @Override
    public long getLong(int columnIndex) throws SQLException {
        Logger.log("getLong " + columnIndex);
        return (long) result.get(position).get(columnIndex);
    }

    @Override
    public float getFloat(int columnIndex) throws SQLException {
        Logger.log("getFloat");
        return 0;
    }

    @Override
    public double getDouble(int columnIndex) throws SQLException {
        Logger.log("getDouble");
        return 0;
    }

    @Override
    public BigDecimal getBigDecimal(int columnIndex, int scale) throws SQLException {
        Logger.log("getBigDecimal");
        return null;
    }

    @Override
    public byte[] getBytes(int columnIndex) throws SQLException {
        Logger.log("[] ");
        return new byte[0];
    }

    @Override
    public Date getDate(int columnIndex) throws SQLException {
        Logger.log("getDate");
        return null;
    }

    @Override
    public Time getTime(int columnIndex) throws SQLException {
        Logger.log("getTime");
        return null;
    }

    @Override
    public Timestamp getTimestamp(int columnIndex) throws SQLException {
        Logger.log("getTimestamp");
        return null;
    }

    @Override
    public InputStream getAsciiStream(int columnIndex) throws SQLException {
        Logger.log("getAsciiStream");
        return null;
    }

    @Override
    public InputStream getUnicodeStream(int columnIndex) throws SQLException {
        Logger.log("getUnicodeStream");
        return null;
    }

    @Override
    public InputStream getBinaryStream(int columnIndex) throws SQLException {
        Logger.log("getBinaryStream");
        return null;
    }

    @Override
    public String getString(String columnLabel) throws SQLException {
        Logger.log("getString ---- " + columnLabel);
        return (String) result.get(position).get(columnLabel);
    }

    @Override
    public boolean getBoolean(String columnLabel) throws SQLException {
        Logger.log("getBoolean - " + columnLabel);
        int value = (int) result.get(position).get(columnLabel);
        return (value == 1);
    }

    @Override
    public byte getByte(String columnLabel) throws SQLException {
        Logger.log("getByte");
        return 0;
    }

    @Override
    public short getShort(String columnLabel) throws SQLException {
        Logger.log("getShort");
        return 0;
    }

    @Override
    public int getInt(String columnLabel) throws SQLException {
        int out = (int) result.get(position).get(columnLabel);
        Logger.log("getInt ---- " + columnLabel + " ----- " + out);
        return out;
    }

    @Override
    public long getLong(String columnLabel) throws SQLException {
        Logger.log("getLong " + columnLabel);
        return (long) result.get(position).get(columnLabel);
    }

    @Override
    public float getFloat(String columnLabel) throws SQLException {
        Logger.log("getFloat");
        return 0;
    }

    @Override
    public double getDouble(String columnLabel) throws SQLException {
        Logger.log("getDouble");
        return 0;
    }

    @Override
    public BigDecimal getBigDecimal(String columnLabel, int scale) throws SQLException {
        Logger.log("getBigDecimal");
        return null;
    }

    @Override
    public byte[] getBytes(String columnLabel) throws SQLException {
        Logger.log("[] ");
        return new byte[0];
    }

    @Override
    public Date getDate(String columnLabel) throws SQLException {
        Logger.log("getDate");
        ZonedDateTime zdt = (ZonedDateTime) result.get(position).get(columnLabel);
        return new Date(zdt.toInstant().toEpochMilli());
    }

    @Override
    public Time getTime(String columnLabel) throws SQLException {
        Logger.log("getTime");
        return null;
    }

    @Override
    public Timestamp getTimestamp(String columnLabel) throws SQLException {
        Logger.log("getTimestamp");
        return null;
    }

    @Override
    public InputStream getAsciiStream(String columnLabel) throws SQLException {
        Logger.log("getAsciiStream");
        return null;
    }

    @Override
    public InputStream getUnicodeStream(String columnLabel) throws SQLException {
        Logger.log("getUnicodeStream");
        return null;
    }

    @Override
    public InputStream getBinaryStream(String columnLabel) throws SQLException {
        Logger.log("getBinaryStream");
        return null;
    }

    @Override
    public SQLWarning getWarnings() throws SQLException {
        Logger.log("getWarnings");
        return null;
    }

    @Override
    public void clearWarnings() throws SQLException {
        Logger.log("clearWarnings");

    }

    @Override
    public String getCursorName() throws SQLException {
        Logger.log("getCursorName");
        return null;
    }

    @Override
    public ResultSetMetaData getMetaData() throws SQLException {
        Logger.log("getMetaData");
        return null;
    }

    @Override
    public Object getObject(int columnIndex) throws SQLException {
        Logger.log("getObject");
        return null;
    }

    @Override
    public Object getObject(String columnLabel) throws SQLException {
        Logger.log("getObject");
        return null;
    }

    @Override
    public int findColumn(String columnLabel) throws SQLException {
        Logger.log("findColumn");
        return 0;
    }

    @Override
    public Reader getCharacterStream(int columnIndex) throws SQLException {
        Logger.log("getCharacterStream");
        return null;
    }

    @Override
    public Reader getCharacterStream(String columnLabel) throws SQLException {
        Logger.log("getCharacterStream");
        return null;
    }

    @Override
    public BigDecimal getBigDecimal(int columnIndex) throws SQLException {
        Logger.log("getBigDecimal");
        return null;
    }

    @Override
    public BigDecimal getBigDecimal(String columnLabel) throws SQLException {
        Logger.log("getBigDecimal");
        return null;
    }

    @Override
    public boolean isBeforeFirst() throws SQLException {
        Logger.log("isBeforeFirst");
        return false;
    }

    @Override
    public boolean isAfterLast() throws SQLException {
        Logger.log("isAfterLast");
        return false;
    }

    @Override
    public boolean isFirst() throws SQLException {
        Logger.log("isFirst");
        return false;
    }

    @Override
    public boolean isLast() throws SQLException {
        Logger.log("isLast");
        return false;
    }

    @Override
    public void beforeFirst() throws SQLException {
        Logger.log("beforeFirst");

    }

    @Override
    public void afterLast() throws SQLException {
        Logger.log("afterLast");

    }

    @Override
    public boolean first() throws SQLException {
        Logger.log("first");
        return false;
    }

    @Override
    public boolean last() throws SQLException {
        Logger.log("last");
        return false;
    }

    @Override
    public int getRow() throws SQLException {
        Logger.log("getRow");
        return 0;
    }

    @Override
    public boolean absolute(int row) throws SQLException {
        Logger.log("absolute");
        return false;
    }

    @Override
    public boolean relative(int rows) throws SQLException {
        Logger.log("relative");
        return false;
    }

    @Override
    public boolean previous() throws SQLException {
        Logger.log("previous");
        return false;
    }

    @Override
    public void setFetchDirection(int direction) throws SQLException {
        Logger.log("setFetchDirection");

    }

    @Override
    public int getFetchDirection() throws SQLException {
        Logger.log("getFetchDirection");
        return 0;
    }

    @Override
    public void setFetchSize(int rows) throws SQLException {
        Logger.log("setFetchSize");

    }

    @Override
    public int getFetchSize() throws SQLException {
        Logger.log("getFetchSize");
        return 0;
    }

    @Override
    public int getType() throws SQLException {
        Logger.log("getType");
        return 0;
    }

    @Override
    public int getConcurrency() throws SQLException {
        Logger.log("getConcurrency");
        return 0;
    }

    @Override
    public boolean rowUpdated() throws SQLException {
        Logger.log("rowUpdated");
        return false;
    }

    @Override
    public boolean rowInserted() throws SQLException {
        Logger.log("rowInserted");
        return false;
    }

    @Override
    public boolean rowDeleted() throws SQLException {
        Logger.log("rowDeleted");
        return false;
    }

    @Override
    public void updateNull(int columnIndex) throws SQLException {
        Logger.log("updateNull");

    }

    @Override
    public void updateBoolean(int columnIndex, boolean x) throws SQLException {
        Logger.log("updateBoolean");

    }

    @Override
    public void updateByte(int columnIndex, byte x) throws SQLException {
        Logger.log("updateByte");

    }

    @Override
    public void updateShort(int columnIndex, short x) throws SQLException {
        Logger.log("updateShort");

    }

    @Override
    public void updateInt(int columnIndex, int x) throws SQLException {
        Logger.log("updateInt");

    }

    @Override
    public void updateLong(int columnIndex, long x) throws SQLException {
        Logger.log("updateLong");

    }

    @Override
    public void updateFloat(int columnIndex, float x) throws SQLException {
        Logger.log("updateFloat");

    }

    @Override
    public void updateDouble(int columnIndex, double x) throws SQLException {
        Logger.log("updateDouble");

    }

    @Override
    public void updateBigDecimal(int columnIndex, BigDecimal x) throws SQLException {
        Logger.log("updateBigDecimal");

    }

    @Override
    public void updateString(int columnIndex, String x) throws SQLException {
        Logger.log("updateString");

    }

    @Override
    public void updateBytes(int columnIndex, byte[] x) throws SQLException {
        Logger.log("updateBytes");

    }

    @Override
    public void updateDate(int columnIndex, Date x) throws SQLException {
        Logger.log("updateDate");

    }

    @Override
    public void updateTime(int columnIndex, Time x) throws SQLException {
        Logger.log("updateTime");

    }

    @Override
    public void updateTimestamp(int columnIndex, Timestamp x) throws SQLException {
        Logger.log("updateTimestamp");

    }

    @Override
    public void updateAsciiStream(int columnIndex, InputStream x, int length) throws SQLException {
        Logger.log("updateAsciiStream");

    }

    @Override
    public void updateBinaryStream(int columnIndex, InputStream x, int length) throws SQLException {
        Logger.log("updateBinaryStream");

    }

    @Override
    public void updateCharacterStream(int columnIndex, Reader x, int length) throws SQLException {
        Logger.log("updateCharacterStream");

    }

    @Override
    public void updateObject(int columnIndex, Object x, int scaleOrLength) throws SQLException {
        Logger.log("updateObject");

    }

    @Override
    public void updateObject(int columnIndex, Object x) throws SQLException {
        Logger.log("updateObject");

    }

    @Override
    public void updateNull(String columnLabel) throws SQLException {
        Logger.log("updateNull");

    }

    @Override
    public void updateBoolean(String columnLabel, boolean x) throws SQLException {
        Logger.log("updateBoolean");

    }

    @Override
    public void updateByte(String columnLabel, byte x) throws SQLException {
        Logger.log("updateByte");

    }

    @Override
    public void updateShort(String columnLabel, short x) throws SQLException {
        Logger.log("updateShort");

    }

    @Override
    public void updateInt(String columnLabel, int x) throws SQLException {
        Logger.log("updateInt");

    }

    @Override
    public void updateLong(String columnLabel, long x) throws SQLException {
        Logger.log("updateLong");

    }

    @Override
    public void updateFloat(String columnLabel, float x) throws SQLException {
        Logger.log("updateFloat");

    }

    @Override
    public void updateDouble(String columnLabel, double x) throws SQLException {
        Logger.log("updateDouble");

    }

    @Override
    public void updateBigDecimal(String columnLabel, BigDecimal x) throws SQLException {
        Logger.log("updateBigDecimal");

    }

    @Override
    public void updateString(String columnLabel, String x) throws SQLException {
        Logger.log("updateString");

    }

    @Override
    public void updateBytes(String columnLabel, byte[] x) throws SQLException {
        Logger.log("updateBytes");

    }

    @Override
    public void updateDate(String columnLabel, Date x) throws SQLException {
        Logger.log("updateDate");

    }

    @Override
    public void updateTime(String columnLabel, Time x) throws SQLException {
        Logger.log("updateTime");

    }

    @Override
    public void updateTimestamp(String columnLabel, Timestamp x) throws SQLException {
        Logger.log("updateTimestamp");

    }

    @Override
    public void updateAsciiStream(String columnLabel, InputStream x, int length) throws SQLException {
        Logger.log("updateAsciiStream");

    }

    @Override
    public void updateBinaryStream(String columnLabel, InputStream x, int length) throws SQLException {
        Logger.log("updateBinaryStream");

    }

    @Override
    public void updateCharacterStream(String columnLabel, Reader reader, int length) throws SQLException {
        Logger.log("updateCharacterStream");

    }

    @Override
    public void updateObject(String columnLabel, Object x, int scaleOrLength) throws SQLException {
        Logger.log("updateObject");

    }

    @Override
    public void updateObject(String columnLabel, Object x) throws SQLException {
        Logger.log("updateObject");

    }

    @Override
    public void insertRow() throws SQLException {
        Logger.log("insertRow");

    }

    @Override
    public void updateRow() throws SQLException {
        Logger.log("updateRow");

    }

    @Override
    public void deleteRow() throws SQLException {
        Logger.log("deleteRow");

    }

    @Override
    public void refreshRow() throws SQLException {
        Logger.log("refreshRow");

    }

    @Override
    public void cancelRowUpdates() throws SQLException {
        Logger.log("cancelRowUpdates");

    }

    @Override
    public void moveToInsertRow() throws SQLException {
        Logger.log("moveToInsertRow");

    }

    @Override
    public void moveToCurrentRow() throws SQLException {
        Logger.log("moveToCurrentRow");

    }

    @Override
    public Statement getStatement() throws SQLException {
        Logger.log("getStatement");
        return null;
    }

    @Override
    public Object getObject(int columnIndex, Map<String, Class<?>> map) throws SQLException {
        Logger.log("getObject");
        return null;
    }

    @Override
    public Ref getRef(int columnIndex) throws SQLException {
        Logger.log("getRef");
        return null;
    }

    @Override
    public Blob getBlob(int columnIndex) throws SQLException {
        Logger.log("getBlob");
        return null;
    }

    @Override
    public Clob getClob(int columnIndex) throws SQLException {
        Logger.log("getClob");
        return null;
    }

    @Override
    public Array getArray(int columnIndex) throws SQLException {
        Logger.log("getArray");
        return null;
    }

    @Override
    public Object getObject(String columnLabel, Map<String, Class<?>> map) throws SQLException {
        Logger.log("getObject");
        return null;
    }

    @Override
    public Ref getRef(String columnLabel) throws SQLException {
        Logger.log("getRef");
        return null;
    }

    @Override
    public Blob getBlob(String columnLabel) throws SQLException {
        Logger.log("getBlob");
        return null;
    }

    @Override
    public Clob getClob(String columnLabel) throws SQLException {
        Logger.log("getClob");
        return null;
    }

    @Override
    public Array getArray(String columnLabel) throws SQLException {
        Logger.log("getArray");
        return null;
    }

    @Override
    public Date getDate(int columnIndex, Calendar cal) throws SQLException {
        Logger.log("getDate");
        return null;
    }

    @Override
    public Date getDate(String columnLabel, Calendar cal) throws SQLException {
        Logger.log("getDate");
        return null;
    }

    @Override
    public Time getTime(int columnIndex, Calendar cal) throws SQLException {
        Logger.log("getTime");
        return null;
    }

    @Override
    public Time getTime(String columnLabel, Calendar cal) throws SQLException {
        Logger.log("getTime");
        return null;
    }

    @Override
    public Timestamp getTimestamp(int columnIndex, Calendar cal) throws SQLException {
        Logger.log("getTimestamp");
        return null;
    }

    @Override
    public Timestamp getTimestamp(String columnLabel, Calendar cal) throws SQLException {
        Logger.log("getTimestamp");
        return null;
    }

    @Override
    public URL getURL(int columnIndex) throws SQLException {
        Logger.log("getURL");
        return null;
    }

    @Override
    public URL getURL(String columnLabel) throws SQLException {
        Logger.log("getURL");
        return null;
    }

    @Override
    public void updateRef(int columnIndex, Ref x) throws SQLException {
        Logger.log("updateRef");

    }

    @Override
    public void updateRef(String columnLabel, Ref x) throws SQLException {
        Logger.log("updateRef");

    }

    @Override
    public void updateBlob(int columnIndex, Blob x) throws SQLException {
        Logger.log("updateBlob");

    }

    @Override
    public void updateBlob(String columnLabel, Blob x) throws SQLException {
        Logger.log("updateBlob");

    }

    @Override
    public void updateClob(int columnIndex, Clob x) throws SQLException {
        Logger.log("updateClob");

    }

    @Override
    public void updateClob(String columnLabel, Clob x) throws SQLException {
        Logger.log("updateClob");

    }

    @Override
    public void updateArray(int columnIndex, Array x) throws SQLException {
        Logger.log("updateArray");

    }

    @Override
    public void updateArray(String columnLabel, Array x) throws SQLException {
        Logger.log("updateArray");

    }

    @Override
    public RowId getRowId(int columnIndex) throws SQLException {
        Logger.log("getRowId");
        return null;
    }

    @Override
    public RowId getRowId(String columnLabel) throws SQLException {
        Logger.log("getRowId");
        return null;
    }

    @Override
    public void updateRowId(int columnIndex, RowId x) throws SQLException {
        Logger.log("updateRowId");

    }

    @Override
    public void updateRowId(String columnLabel, RowId x) throws SQLException {
        Logger.log("updateRowId");

    }

    @Override
    public int getHoldability() throws SQLException {
        Logger.log("getHoldability");
        return 0;
    }

    @Override
    public boolean isClosed() throws SQLException {
        Logger.log("isClosed");
        return false;
    }

    @Override
    public void updateNString(int columnIndex, String nString) throws SQLException {
        Logger.log("updateNString");

    }

    @Override
    public void updateNString(String columnLabel, String nString) throws SQLException {
        Logger.log("updateNString");

    }

    @Override
    public void updateNClob(int columnIndex, NClob nClob) throws SQLException {
        Logger.log("updateNClob");

    }

    @Override
    public void updateNClob(String columnLabel, NClob nClob) throws SQLException {
        Logger.log("updateNClob");

    }

    @Override
    public NClob getNClob(int columnIndex) throws SQLException {
        Logger.log("getNClob");
        return null;
    }

    @Override
    public NClob getNClob(String columnLabel) throws SQLException {
        Logger.log("getNClob");
        return null;
    }

    @Override
    public SQLXML getSQLXML(int columnIndex) throws SQLException {
        Logger.log("getSQLXML");
        return null;
    }

    @Override
    public SQLXML getSQLXML(String columnLabel) throws SQLException {
        Logger.log("getSQLXML");
        return null;
    }

    @Override
    public void updateSQLXML(int columnIndex, SQLXML xmlObject) throws SQLException {
        Logger.log("updateSQLXML");

    }

    @Override
    public void updateSQLXML(String columnLabel, SQLXML xmlObject) throws SQLException {
        Logger.log("updateSQLXML");

    }

    @Override
    public String getNString(int columnIndex) throws SQLException {
        Logger.log("getNString");
        return null;
    }

    @Override
    public String getNString(String columnLabel) throws SQLException {
        Logger.log("getNString");
        return null;
    }

    @Override
    public Reader getNCharacterStream(int columnIndex) throws SQLException {
        Logger.log("getNCharacterStream");
        return null;
    }

    @Override
    public Reader getNCharacterStream(String columnLabel) throws SQLException {
        Logger.log("getNCharacterStream");
        return null;
    }

    @Override
    public void updateNCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
        Logger.log("updateNCharacterStream");

    }

    @Override
    public void updateNCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
        Logger.log("updateNCharacterStream");

    }

    @Override
    public void updateAsciiStream(int columnIndex, InputStream x, long length) throws SQLException {
        Logger.log("updateAsciiStream");

    }

    @Override
    public void updateBinaryStream(int columnIndex, InputStream x, long length) throws SQLException {
        Logger.log("updateBinaryStream");

    }

    @Override
    public void updateCharacterStream(int columnIndex, Reader x, long length) throws SQLException {
        Logger.log("updateCharacterStream");

    }

    @Override
    public void updateAsciiStream(String columnLabel, InputStream x, long length) throws SQLException {
        Logger.log("updateAsciiStream");

    }

    @Override
    public void updateBinaryStream(String columnLabel, InputStream x, long length) throws SQLException {
        Logger.log("updateBinaryStream");

    }

    @Override
    public void updateCharacterStream(String columnLabel, Reader reader, long length) throws SQLException {
        Logger.log("updateCharacterStream");

    }

    @Override
    public void updateBlob(int columnIndex, InputStream inputStream, long length) throws SQLException {
        Logger.log("updateBlob");

    }

    @Override
    public void updateBlob(String columnLabel, InputStream inputStream, long length) throws SQLException {
        Logger.log("updateBlob");

    }

    @Override
    public void updateClob(int columnIndex, Reader reader, long length) throws SQLException {
        Logger.log("updateClob");

    }

    @Override
    public void updateClob(String columnLabel, Reader reader, long length) throws SQLException {
        Logger.log("updateClob");

    }

    @Override
    public void updateNClob(int columnIndex, Reader reader, long length) throws SQLException {
        Logger.log("updateNClob");

    }

    @Override
    public void updateNClob(String columnLabel, Reader reader, long length) throws SQLException {
        Logger.log("updateNClob");

    }

    @Override
    public void updateNCharacterStream(int columnIndex, Reader x) throws SQLException {
        Logger.log("updateNCharacterStream");

    }

    @Override
    public void updateNCharacterStream(String columnLabel, Reader reader) throws SQLException {
        Logger.log("updateNCharacterStream");

    }

    @Override
    public void updateAsciiStream(int columnIndex, InputStream x) throws SQLException {
        Logger.log("updateAsciiStream");

    }

    @Override
    public void updateBinaryStream(int columnIndex, InputStream x) throws SQLException {
        Logger.log("updateBinaryStream");

    }

    @Override
    public void updateCharacterStream(int columnIndex, Reader x) throws SQLException {
        Logger.log("updateCharacterStream");

    }

    @Override
    public void updateAsciiStream(String columnLabel, InputStream x) throws SQLException {
        Logger.log("updateAsciiStream");

    }

    @Override
    public void updateBinaryStream(String columnLabel, InputStream x) throws SQLException {
        Logger.log("updateBinaryStream");

    }

    @Override
    public void updateCharacterStream(String columnLabel, Reader reader) throws SQLException {
        Logger.log("updateCharacterStream");

    }

    @Override
    public void updateBlob(int columnIndex, InputStream inputStream) throws SQLException {
        Logger.log("updateBlob");

    }

    @Override
    public void updateBlob(String columnLabel, InputStream inputStream) throws SQLException {
        Logger.log("updateBlob");

    }

    @Override
    public void updateClob(int columnIndex, Reader reader) throws SQLException {
        Logger.log("updateClob");

    }

    @Override
    public void updateClob(String columnLabel, Reader reader) throws SQLException {
        Logger.log("updateClob");

    }

    @Override
    public void updateNClob(int columnIndex, Reader reader) throws SQLException {
        Logger.log("updateNClob");

    }

    @Override
    public void updateNClob(String columnLabel, Reader reader) throws SQLException {
        Logger.log("updateNClob");

    }

    @Override
    public <T> T getObject(int columnIndex, Class<T> type) throws SQLException {
        Logger.log("T");
        return null;
    }

    @Override
    public <T> T getObject(String columnLabel, Class<T> type) throws SQLException {
        Logger.log("T");
        return null;
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        Logger.log("T");
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        Logger.log("isWrapperFor");
        return false;
    }
}
