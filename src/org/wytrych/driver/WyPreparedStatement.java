package org.wytrych.driver;

import org.wytrych.QueryResult.QueryResult;
import org.wytrych.QueryResult.QueryStatement;

import java.io.*;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.*;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Queue;

public class WyPreparedStatement implements PreparedStatement {
    private String sql;
    private final ObjectInputStream in;
    private final ObjectOutputStream out;
    private final Queue<Object> parameters = new LinkedList<>();

    WyPreparedStatement(String sql, ObjectInputStream in, ObjectOutputStream out) {
        Logger.log("Create WyPreparedStatement");
        this.sql = sql;
        this.in = in;
        this.out = out;
    }

    private void setParameter(Object x) {
        parameters.add(x);
    }

    @Override
    public ResultSet executeQuery() throws SQLException {
        Logger.log("executeQuery XX");
        QueryStatement query = new QueryStatement(sql, parameters);
        try {
            out.writeObject(query);
        } catch (IOException e) {
            e.printStackTrace();
        }
        QueryResult result;
        try {
            result = (QueryResult) in.readObject();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }

        return new WyResultSet(result);
    }

    @Override
    public int executeUpdate() throws SQLException {
        Logger.log("executeUpdate");
        QueryStatement query = new QueryStatement(sql, parameters);
        try {
            out.writeObject(query);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    public void setNull(int parameterIndex, int sqlType) throws SQLException {
        Logger.log("setNull");

    }

    @Override
    public void setBoolean(int parameterIndex, boolean x) throws SQLException {
        Object value = (x) ? 1 : 0;
        setParameter(value);
        Logger.log("setBoolean: " + x);
    }

    @Override
    public void setByte(int parameterIndex, byte x) throws SQLException {
        Logger.log("setByte");

    }

    @Override
    public void setShort(int parameterIndex, short x) throws SQLException {
        Logger.log("setShort");

    }

    @Override
    public void setInt(int parameterIndex, int x) throws SQLException {
        setParameter(x);
        Logger.log("setInt: " + x);
    }

    @Override
    public void setLong(int parameterIndex, long x) throws SQLException {
        setParameter(x);
        Logger.log("setLong: " + x);
    }

    @Override
    public void setFloat(int parameterIndex, float x) throws SQLException {
        Logger.log("setFloat");
    }

    @Override
    public void setDouble(int parameterIndex, double x) throws SQLException {
        Logger.log("setDouble");
    }

    @Override
    public void setBigDecimal(int parameterIndex, BigDecimal x) throws SQLException {
        Logger.log("setBigDecimal");
    }

    @Override
    public void setString(int parameterIndex, String x) throws SQLException {
        x = x.replaceAll("\"", "");
        setParameter(x);
        String quotedString = "\"" + x + "\"";
        Logger.log("setString: " + quotedString);
    }

    @Override
    public void setBytes(int parameterIndex, byte[] x) throws SQLException {
        Logger.log("setBytes");

    }

    @Override
    public void setDate(int parameterIndex, Date x) throws SQLException {
        setParameter(x);
        Logger.log("setDate: " + x);

    }

    @Override
    public void setTime(int parameterIndex, Time x) throws SQLException {
        Logger.log("setTime");

    }

    @Override
    public void setTimestamp(int parameterIndex, Timestamp x) throws SQLException {
        Logger.log("setTimestamp");

    }

    @Override
    public void setAsciiStream(int parameterIndex, InputStream x, int length) throws SQLException {
        Logger.log("setAsciiStream");

    }

    @Override
    public void setUnicodeStream(int parameterIndex, InputStream x, int length) throws SQLException {
        Logger.log("setUnicodeStream");

    }

    @Override
    public void setBinaryStream(int parameterIndex, InputStream x, int length) throws SQLException {
        Logger.log("setBinaryStream");

    }

    @Override
    public void clearParameters() throws SQLException {
        Logger.log("clearParameters");

    }

    @Override
    public void setObject(int parameterIndex, Object x, int targetSqlType) throws SQLException {
        Logger.log("setObject");

    }

    @Override
    public void setObject(int parameterIndex, Object x) throws SQLException {
        Logger.log("setObject");

    }

    @Override
    public boolean execute() throws SQLException {
        Logger.log("execute");
        return false;
    }

    @Override
    public void addBatch() throws SQLException {
        Logger.log("addBatch");

    }

    @Override
    public void setCharacterStream(int parameterIndex, Reader reader, int length) throws SQLException {
        Logger.log("setCharacterStream");

    }

    @Override
    public void setRef(int parameterIndex, Ref x) throws SQLException {
        Logger.log("setRef");

    }

    @Override
    public void setBlob(int parameterIndex, Blob x) throws SQLException {
        Logger.log("setBlob");

    }

    @Override
    public void setClob(int parameterIndex, Clob x) throws SQLException {
        Logger.log("setClob");

    }

    @Override
    public void setArray(int parameterIndex, Array x) throws SQLException {
        Logger.log("setArray");

    }

    @Override
    public ResultSetMetaData getMetaData() throws SQLException {
        Logger.log("getMetaData");
        return null;
    }

    @Override
    public void setDate(int parameterIndex, Date x, Calendar cal) throws SQLException {
        Logger.log("setDate");

    }

    @Override
    public void setTime(int parameterIndex, Time x, Calendar cal) throws SQLException {
        Logger.log("setTime");

    }

    @Override
    public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal) throws SQLException {
        Logger.log("setTimestamp");

    }

    @Override
    public void setNull(int parameterIndex, int sqlType, String typeName) throws SQLException {
        Logger.log("setNull");

    }

    @Override
    public void setURL(int parameterIndex, URL x) throws SQLException {
        Logger.log("setURL");

    }

    @Override
    public ParameterMetaData getParameterMetaData() throws SQLException {
        Logger.log("getParameterMetaData");
        return null;
    }

    @Override
    public void setRowId(int parameterIndex, RowId x) throws SQLException {
        Logger.log("setRowId");

    }

    @Override
    public void setNString(int parameterIndex, String value) throws SQLException {
        Logger.log("setNString");

    }

    @Override
    public void setNCharacterStream(int parameterIndex, Reader value, long length) throws SQLException {
        Logger.log("setNCharacterStream");

    }

    @Override
    public void setNClob(int parameterIndex, NClob value) throws SQLException {
        Logger.log("setNClob");

    }

    @Override
    public void setClob(int parameterIndex, Reader reader, long length) throws SQLException {
        Logger.log("setClob");

    }

    @Override
    public void setBlob(int parameterIndex, InputStream inputStream, long length) throws SQLException {
        Logger.log("setBlob");

    }

    @Override
    public void setNClob(int parameterIndex, Reader reader, long length) throws SQLException {
        Logger.log("setNClob");

    }

    @Override
    public void setSQLXML(int parameterIndex, SQLXML xmlObject) throws SQLException {
        Logger.log("setSQLXML");

    }

    @Override
    public void setObject(int parameterIndex, Object x, int targetSqlType, int scaleOrLength) throws SQLException {
        Logger.log("setObject");

    }

    @Override
    public void setAsciiStream(int parameterIndex, InputStream x, long length) throws SQLException {
        Logger.log("setAsciiStream");

    }

    @Override
    public void setBinaryStream(int parameterIndex, InputStream x, long length) throws SQLException {
        Logger.log("setBinaryStream");

    }

    @Override
    public void setCharacterStream(int parameterIndex, Reader reader, long length) throws SQLException {
        Logger.log("setCharacterStream");

    }

    @Override
    public void setAsciiStream(int parameterIndex, InputStream x) throws SQLException {
        Logger.log("setAsciiStream");

    }

    @Override
    public void setBinaryStream(int parameterIndex, InputStream x) throws SQLException {
        Logger.log("setBinaryStream");

    }

    @Override
    public void setCharacterStream(int parameterIndex, Reader reader) throws SQLException {
        Logger.log("setCharacterStream");

    }

    @Override
    public void setNCharacterStream(int parameterIndex, Reader value) throws SQLException {
        Logger.log("setNCharacterStream");

    }

    @Override
    public void setClob(int parameterIndex, Reader reader) throws SQLException {
        Logger.log("setClob");

    }

    @Override
    public void setBlob(int parameterIndex, InputStream inputStream) throws SQLException {
        Logger.log("setBlob");

    }

    @Override
    public void setNClob(int parameterIndex, Reader reader) throws SQLException {
        Logger.log("setNClob");

    }

    @Override
    public ResultSet executeQuery(String sql) throws SQLException {
        Logger.log("executeQuery");
        return null;
    }

    @Override
    public int executeUpdate(String sql) throws SQLException {
        Logger.log("executeUpdate");
        return 0;
    }

    @Override
    public void close() throws SQLException {
        Logger.log("close --- ooooooo");

    }

    @Override
    public int getMaxFieldSize() throws SQLException {
        Logger.log("getMaxFieldSize");
        return 0;
    }

    @Override
    public void setMaxFieldSize(int max) throws SQLException {
        Logger.log("setMaxFieldSize");

    }

    @Override
    public int getMaxRows() throws SQLException {
        Logger.log("getMaxRows");
        return 10;
    }

    @Override
    public void setMaxRows(int max) throws SQLException {
        Logger.log("setMaxRows " + max);

    }

    @Override
    public void setEscapeProcessing(boolean enable) throws SQLException {
        Logger.log("setEscapeProcessing");

    }

    @Override
    public int getQueryTimeout() throws SQLException {
        Logger.log("getQueryTimeout");
        return 1000;
    }

    @Override
    public void setQueryTimeout(int seconds) throws SQLException {
        Logger.log("setQueryTimeout " + seconds);

    }

    @Override
    public void cancel() throws SQLException {
        Logger.log("cancel");

    }

    @Override
    public SQLWarning getWarnings() throws SQLException {
        Logger.log("getWarnings");
        return null;
    }

    @Override
    public void clearWarnings() throws SQLException {
        Logger.log("clearWarnings");

    }

    @Override
    public void setCursorName(String name) throws SQLException {
        Logger.log("setCursorName");

    }

    @Override
    public boolean execute(String sql) throws SQLException {
        Logger.log("execute");
        return false;
    }

    @Override
    public ResultSet getResultSet() throws SQLException {
        Logger.log("getResultSet");
        return null;
    }

    @Override
    public int getUpdateCount() throws SQLException {
        Logger.log("getUpdateCount");
        return 0;
    }

    @Override
    public boolean getMoreResults() throws SQLException {
        Logger.log("getMoreResults");
        return false;
    }

    @Override
    public void setFetchDirection(int direction) throws SQLException {
        Logger.log("setFetchDirection");

    }

    @Override
    public int getFetchDirection() throws SQLException {
        Logger.log("getFetchDirection");
        return 0;
    }

    @Override
    public void setFetchSize(int rows) throws SQLException {
        Logger.log("setFetchSize");

    }

    @Override
    public int getFetchSize() throws SQLException {
        Logger.log("getFetchSize");
        return 0;
    }

    @Override
    public int getResultSetConcurrency() throws SQLException {
        Logger.log("getResultSetConcurrency");
        return 0;
    }

    @Override
    public int getResultSetType() throws SQLException {
        Logger.log("getResultSetType");
        return 0;
    }

    @Override
    public void addBatch(String sql) throws SQLException {
        Logger.log("addBatch");

    }

    @Override
    public void clearBatch() throws SQLException {
        Logger.log("clearBatch");

    }

    @Override
    public int[] executeBatch() throws SQLException {
        Logger.log("[] ");
        return new int[0];
    }

    @Override
    public Connection getConnection() throws SQLException {
        Logger.log("getConnection");
        return null;
    }

    @Override
    public boolean getMoreResults(int current) throws SQLException {
        Logger.log("getMoreResults");
        return false;
    }

    @Override
    public ResultSet getGeneratedKeys() throws SQLException {
        Logger.log("getGeneratedKeys");
        return null;
    }

    @Override
    public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
        Logger.log("executeUpdate");
        return 0;
    }

    @Override
    public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
        Logger.log("executeUpdate");
        return 0;
    }

    @Override
    public int executeUpdate(String sql, String[] columnNames) throws SQLException {
        Logger.log("executeUpdate");
        return 0;
    }

    @Override
    public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
        Logger.log("execute");
        return false;
    }

    @Override
    public boolean execute(String sql, int[] columnIndexes) throws SQLException {
        Logger.log("execute");
        return false;
    }

    @Override
    public boolean execute(String sql, String[] columnNames) throws SQLException {
        Logger.log("execute");
        return false;
    }

    @Override
    public int getResultSetHoldability() throws SQLException {
        Logger.log("getResultSetHoldability");
        return 0;
    }

    @Override
    public boolean isClosed() throws SQLException {
        Logger.log("isClosed");
        return false;
    }

    @Override
    public void setPoolable(boolean poolable) throws SQLException {
        Logger.log("setPoolable");

    }

    @Override
    public boolean isPoolable() throws SQLException {
        Logger.log("isPoolable");
        return false;
    }

    @Override
    public void closeOnCompletion() throws SQLException {
        Logger.log("closeOnCompletion");

    }

    @Override
    public boolean isCloseOnCompletion() throws SQLException {
        Logger.log("isCloseOnCompletion");
        return false;
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        Logger.log("T");
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        Logger.log("isWrapperFor");
        return false;
    }
}
