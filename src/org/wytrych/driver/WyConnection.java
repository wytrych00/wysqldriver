package org.wytrych.driver;

import org.wytrych.QueryResult.QueryStatement;

import java.io.*;
import java.net.Socket;
import java.sql.*;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WyConnection implements Connection {
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private boolean autoCommit = true;
    private Pattern urlPattern = Pattern.compile(".*//(\\w*)/.*");

    WyConnection(String url) throws IOException, ClassNotFoundException {
        Matcher urlMatcher = urlPattern.matcher(url);
        urlMatcher.find();
        String host = urlMatcher.group(1);
        Integer port = 9009;

        Logger.log("WyConnection started");
        Socket socket = new Socket(host, port);
        Logger.log("Create socket");
        out = new ObjectOutputStream(socket.getOutputStream());
        Logger.log("Create out");
        InputStream is = socket.getInputStream();
        Logger.log("Got input stream");
        in = new ObjectInputStream(is);
        Logger.log("Create in");
        in.readObject();
        Logger.log("Finish constructor");
    }

    @Override
    public Statement createStatement() throws SQLException {
        Logger.log("createStatement");
        return null;
    }

    @Override
    public PreparedStatement prepareStatement(String sql) throws SQLException {
        Logger.log("prepareStatement");
        Logger.log(sql);
        return new WyPreparedStatement(sql, in, out);
    }

    @Override
    public CallableStatement prepareCall(String sql) throws SQLException {
        Logger.log("prepareCall");
        return null;
    }

    @Override
    public String nativeSQL(String sql) throws SQLException {
        Logger.log("nativeSQL");
        return null;
    }

    @Override
    public void setAutoCommit(boolean autoCommit) throws SQLException {
        this.autoCommit = autoCommit;
        Logger.log("setAutoCommit " + autoCommit);
    }

    @Override
    public boolean getAutoCommit() throws SQLException {
        Logger.log("getAutoCommit");
        return autoCommit;
    }

    @Override
    public void commit() throws SQLException {
        Logger.log("commit");

    }

    @Override
    public void rollback() throws SQLException {
        Logger.log("rollback");

    }

    @Override
    public void close() throws SQLException {
        try {
            out.writeObject(new QueryStatement("x"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Logger.log("close - zzzzz");

    }

    @Override
    public boolean isClosed() throws SQLException {
        Logger.log("isClosed");
        return false;
    }

    @Override
    public DatabaseMetaData getMetaData() throws SQLException {
        Logger.log("getMetaData");
        return new WyMetaData("localhost");
    }

    @Override
    public void setReadOnly(boolean readOnly) throws SQLException {
        Logger.log("setReadOnly");

    }

    @Override
    public boolean isReadOnly() throws SQLException {
        Logger.log("isReadOnly");
        return false;
    }

    @Override
    public void setCatalog(String catalog) throws SQLException {
        Logger.log("setCatalog");

    }

    @Override
    public String getCatalog() throws SQLException {
        Logger.log("getCatalog");
        return null;
    }

    @Override
    public void setTransactionIsolation(int level) throws SQLException {
        Logger.log("setTransactionIsolation");

    }

    @Override
    public int getTransactionIsolation() throws SQLException {
        Logger.log("getTransactionIsolation");
        return 0;
    }

    @Override
    public SQLWarning getWarnings() throws SQLException {
        Logger.log("getWarnings");
        return null;
    }

    @Override
    public void clearWarnings() throws SQLException {
        Logger.log("clearWarnings");

    }

    @Override
    public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
        Logger.log("createStatement");
        return null;
    }

    @Override
    public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
        Logger.log("prepareStatement");
        return new WyPreparedStatement(sql, in, out);
    }

    @Override
    public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
        Logger.log("prepareCall");
        return null;
    }

    @Override
    public Map<String, Class<?>> getTypeMap() throws SQLException {
        Logger.log("<");
        return null;
    }

    @Override
    public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
        Logger.log("setTypeMap");

    }

    @Override
    public void setHoldability(int holdability) throws SQLException {
        Logger.log("setHoldability");

    }

    @Override
    public int getHoldability() throws SQLException {
        Logger.log("getHoldability");
        return 0;
    }

    @Override
    public Savepoint setSavepoint() throws SQLException {
        Logger.log("setSavepoint");
        return null;
    }

    @Override
    public Savepoint setSavepoint(String name) throws SQLException {
        Logger.log("setSavepoint");
        return null;
    }

    @Override
    public void rollback(Savepoint savepoint) throws SQLException {
        Logger.log("rollback");

    }

    @Override
    public void releaseSavepoint(Savepoint savepoint) throws SQLException {
        Logger.log("releaseSavepoint");

    }

    @Override
    public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
        Logger.log("createStatement");
        return null;
    }

    @Override
    public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
        Logger.log("prepareStatement");
        return new WyPreparedStatement(sql, in, out);
    }

    @Override
    public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
        Logger.log("prepareCall");
        return null;
    }

    @Override
    public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
        Logger.log("prepareStatement");
        return new WyPreparedStatement(sql, in, out);
    }

    @Override
    public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
        Logger.log("prepareStatement");
        return new WyPreparedStatement(sql, in, out);
    }

    @Override
    public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
        Logger.log("prepareStatement");
        return new WyPreparedStatement(sql, in, out);
    }

    @Override
    public Clob createClob() throws SQLException {
        Logger.log("createClob");
        return null;
    }

    @Override
    public Blob createBlob() throws SQLException {
        Logger.log("createBlob");
        return null;
    }

    @Override
    public NClob createNClob() throws SQLException {
        Logger.log("createNClob");
        return null;
    }

    @Override
    public SQLXML createSQLXML() throws SQLException {
        Logger.log("createSQLXML");
        return null;
    }

    @Override
    public boolean isValid(int timeout) throws SQLException {
        Logger.log("isValid");
        return false;
    }

    @Override
    public void setClientInfo(String name, String value) throws SQLClientInfoException {
        Logger.log("setClientInfo");

    }

    @Override
    public void setClientInfo(Properties properties) throws SQLClientInfoException {
        Logger.log("setClientInfo");

    }

    @Override
    public String getClientInfo(String name) throws SQLException {
        Logger.log("getClientInfo");
        return null;
    }

    @Override
    public Properties getClientInfo() throws SQLException {
        Logger.log("getClientInfo");
        return null;
    }

    @Override
    public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
        Logger.log("createArrayOf");
        return null;
    }

    @Override
    public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
        Logger.log("createStruct");
        return null;
    }

    @Override
    public void setSchema(String schema) throws SQLException {
        Logger.log("setSchema");

    }

    @Override
    public String getSchema() throws SQLException {
        Logger.log("getSchema");
        return null;
    }

    @Override
    public void abort(Executor executor) throws SQLException {
        Logger.log("abort");

    }

    @Override
    public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
        Logger.log("setNetworkTimeout");

    }

    @Override
    public int getNetworkTimeout() throws SQLException {
        Logger.log("getNetworkTimeout");
        return 0;
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        Logger.log("uwrap");
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        Logger.log("isWrapperFor");
        return false;
    }
}
