package org.wytrych.driver;

import java.io.IOException;
import java.sql.*;
import java.util.Properties;


public class WySQLDriver implements Driver {

    static {
        try {
            Logger.log("Register driver");
            DriverManager.registerDriver(new WySQLDriver());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Connection connect(String url, Properties info) throws SQLException {
        if (url.contains("mysql"))
            return null;
        Logger.log("DriverStarted");
        Logger.log(url);
        try {
            return new WyConnection(url);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean acceptsURL(String url) throws SQLException {
        return true;
    }

    @Override
    public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
        Logger.log("Getting driver properites");
        return new DriverPropertyInfo[0];
    }

    @Override
    public int getMajorVersion() {
        return 0;
    }

    @Override
    public int getMinorVersion() {
        return 0;
    }

    @Override
    public boolean jdbcCompliant() {
        return false;
    }

    @Override
    public java.util.logging.Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return null;
    }
}
