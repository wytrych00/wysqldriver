package org.wytrych.client;

import org.wytrych.QueryResult.QueryResult;
import org.wytrych.QueryResult.QueryStatement;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class WySQLClient {
    public static void main (String... args) throws IOException, ClassNotFoundException {
        if (args.length == 0) {
            System.out.println("Provide WySQL server hostname as first argument");
            System.exit(1);
        }
        connect(args[0]);
    }

    private static void connect(String host) throws IOException, ClassNotFoundException {
        Integer port = 9009;
        System.out.println("Connecting to: " + host + ":" + port);
        Socket socket = new Socket(host, port);
        ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
        ObjectInputStream in = new ObjectInputStream(socket.getInputStream());

        QueryResult result = (QueryResult)in.readObject();

        Scanner sc = new Scanner(System.in);

        while (!result.status.equals("CLOSE")) {
            if (result.status.equals("ERROR"))
                System.out.println("ERROR: " + result.message);
            else {
                String formattedResult = QueryFormatter.format(result);
                System.out.println(formattedResult);
            }
            System.out.println();
            System.out.print("> ");

            String i = sc.nextLine();
            out.writeObject(new QueryStatement(i));
            result = (QueryResult)in.readObject();
        }

    }
}
