package org.wytrych.client;

import org.wytrych.QueryResult.QueryResult;

import java.util.Map;

public class QueryFormatter {
    private static String output;

    public static String format(QueryResult result) {
        if (result.size() == 0)
            return result.status + ": " + result.message;

        System.out.println(result.size());

        output = "--------------------------------\n";
        for (int i = 0; i < result.size(); i++) {
            Map<String, Object> row = result.result.get(i);
            output = output.concat("|");
            row.forEach((key, value) -> output = output.concat(" " + key + " = " + value + " |"));
            output = output.concat("\n");
        }

        output = output.concat("--------------------------------\n\n");

        return output;

    }
}
