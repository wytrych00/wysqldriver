# WySQL JDBC Driver

A very minimalistic implementation of the JDBC specification to be used with WySQL.
The feature set is very limited, it only provides a PreparedStatement and setting
a few basic types: int, long, string, date, bit (boolean).

To use, start `org.wytrych.WySQL.WyServer.main` first and then run
`org.wytrych.client.WySQLClient` providing the WySQL server host name as the first
and only command line argument.